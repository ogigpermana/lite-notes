package com.landingcasts.litenotes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.landingcasts.litenotes.models.Note;

public class NotesListActivity extends AppCompatActivity {

    private static final String TAG = "NotesListActivity" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_list);

        Note note = new Note("Judul", "Konten","Timestamp");
        // Contoh menampilkan semua object yang kita definisikan diatas
        Log.d(TAG, "onCreate: full catatanku " + note.toString());
        // Contoh pemanggilan object parsial
        Log.d(TAG, "onCreate: catatanku " + note.getTitle());
        Log.d(TAG, "onCreate: catatanku " + note.getContent());
        Log.d(TAG, "onCreate: catatanku " + note.getTimestamp());
    }
}
